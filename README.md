# QR Phone

This is a chrome extension, QR Phone is usefull if you want to compose a phone number on your smartphone.
How does it works?

- Extract phone number from selected text using [regex](https://regex101.com/r/GmDIbg/2) 
- Convert it into a QR code using [QR code generator web API](http://goqr.me/api/)
- You just have to scan this QR code with your smartphone an open the link 🚀

## Setup

1. Download the repository
2. Open Chrome and go to your [extensions](chrome://extensions/)
3. Click on the "Load unpacked extension..." button
4. Select the downloaded directory "qr-phone"