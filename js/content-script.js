/**
 * Stollen from https://regex101.com/r/GmDIbg/2
 */
const PHONE_REGEX = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/mg;

// Watch selection event
document.addEventListener("mouseup", () => {
    const selection = window.getSelection();
    const text = selection.toString();
    // Check if a valid phone number is selected
    if (text && text.match(PHONE_REGEX)) {
        const phone = text.match(PHONE_REGEX)[0];
        pushQrCode(phone);
    }
});

// Add absolute div into the DOM to display selected phone number as QR code
function pushQrCode(phone) {
    const notif = document.createElement('qrPhone-notif');

    const qrCode = document.createElement('img');
    const imgSrc = `https://api.qrserver.com/v1/create-qr-code/?data=${phone}`;
    qrCode.setAttribute('src', imgSrc)
    notif.appendChild(qrCode);

    const title = document.createElement('h1');
    title.innerText = `${phone} 🦖`;
    notif.appendChild(title);

    document.body.appendChild(notif);

    // Remove element
    setTimeout(() => {
        notif.remove();
    }, 5000)
}